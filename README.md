#Introduction

This ***BeautyBot*** is an android application using **Dialogflow** for Haircut booking at beauty salon. Here I am taking reference of **FaSS Beauty Salon Kawasaki**

#Source code

Repo: https://bitbucket.org/vikasdeep/android-beauty-bot/src/master/

BeautySalonAgent : https://drive.google.com/open?id=1GlWstR83V8xR1jghGtt8kvNy749S9sU0


#Reason for the idea

The reason why I made chat bot for Haircut salon is that I recently faced issues in getting hair cut appointment. 
I tried at different salons as well, all have either booking by phone call (which is only during working hours of the salon) or by HotPepper app (which I found bit complex).
So I thought to create a chat bot by which customers can take haircut appointment at any time any day. 


#Reason for platform and programming language

I have worked on many android applications using Java and am comfortable with Java so I used Java as programming language. 
For ChatBot I used Dialogflow's AI API because it is very easy to understand and popular as well.


#Time to complete task

I completed this task in around 16-18 hours in total i.e. around 2 hours per day at night after office work.

Below is breakdown of time:

1. Studied about ChatBot and Dialogflow API
2. Created *BeautySalonAgent* project on Dialogflow
3. Android app side development and Dialogflow API integration
4. Code cleanup and minor changes


#Operating manual

###Project build and installation instructions

1. Clone the project using below command

	`git clone https://vikasdeep@bitbucket.org/vikasdeep/android-beauty-bot.git`

2. Import project to Android Studio and build it
3. Once project is built successfully, install to your device or emulator

###Features of BeautyBot app

1. Chat with salon agent using predefined responses like Initial Message, Shop Business Hours, Address, Booking/Appointment (this is not actual booking, it is just to show demo ) and other small messages
2. Connectivity (Internet and Network) status and indicate using Toast message
3. Error handling for Dialogflow AI API


###Usage of BeautyBot app

1. Send initial message. e.g. "Hello", "Hi How are you?", "Whats up?" etc
2. Ask for business hours. e.g. "When I can come to your shop?", "What are your working hours?", "What are your shop timings" etc
3. Ask for shop address. e.g. "What's your shop?", "What is address of your shop?", "Could you please send your shop address" etc
4. Ask for appointment/booking. e.g. "I want haircut", "Want to book an appointment for haircut", "Could you please book my haircut appointment" etc
5. Tell date and time for booking. e.g. "Tomorrow at 15:30", "June 30th at 16:00", "6:30PM on 1st July" etc
6. Send ending message. e.g. "Thanks", "Great. Thank you!", "Thank you very much" etc

###Usage of BeautySalonAgent only

1. Download the BeautySalonAgent.zip from the google drive link (BeautySalonAgent) given in sourec code section
2. Go to https://console.dialogflow.com/api-client/#/login and login to Dialogflow
3. Go to https://console.dialogflow.com/api-client/#/agents and click **CREATE AGENT**
4. Type agent name of your choice **without any space** e.g. MyTestAgent. Click on **CREATE** button and wait for few seconds for process to be completed
5. By default it will ask you to create Intent. Don't do that but go to the setting by clicking on setting icon to the right of MyTestAgent
6. Go to **Export and Import** tab and click on last option **IMPORT FROM ZIP**
7. Select File i.e. **BeautySalonAgent.zip** and type **IMPORT** in text input area to enable IMPORT button
8. You are done. Now you can test/play with agent only without Android application here.
9. You will find option **Try it now**. Type here anything and you will see the output response below
10. You can try with Google Assistent console as well from **See how it works in Google Assistant.** option

#Unique Selling Point

This is just a demo app and this idea can be converted into a full-fledged product for Japanese 
market not only for Salon but also for other services also like restaurants, dentist, hospitals and many more.


#Difficulty if any

No difficulty faced during this task


#References

Dialogflow AI API : https://console.dialogflow.com/api-client/#/login

Dialogflow Android Client SDK : https://github.com/dialogflow/dialogflow-android-client

FaSS Salon Kawasaki : http://www.fasssalon.com/salonlist/detail.php?qbid=4211

Android Development : https://developer.android.com/
