package com.vikasdeep.beautybot;

/**
 * ChatData class to get data binding with conversation list
 */
public class ChatData {

    public String body;
    public boolean isUser;

    ChatData(String Sender, String Receiver, String messageString, boolean isUSER) {
        body = messageString;
        isUser = isUSER;
    }
}
