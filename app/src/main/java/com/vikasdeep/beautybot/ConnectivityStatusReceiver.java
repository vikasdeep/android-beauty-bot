package com.vikasdeep.beautybot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

/**
 * ConnectivityStatusReceiver is responsible for checking internet and network connectivity status
 */
public class ConnectivityStatusReceiver extends BroadcastReceiver {

    public static final String MY_PREFS_NAME = "launch-check";
    public static final String KEY_NAME = "isLaunchTime";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
            final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            assert connMgr != null;
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

            if (activeNetworkInfo != null) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                boolean isLaunch = sharedPreferences.getBoolean(KEY_NAME, false);

                // this check is to avoid annoying Toast at launch time of app
                if (!isLaunch) {
                    SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putBoolean(KEY_NAME, true).apply();
                } else {
                    // Show toast if Internet or network is connected
                    Toast.makeText(context, activeNetworkInfo.getTypeName() + " " +context.getResources().getString(R.string.connectedMsg), Toast.LENGTH_SHORT).show();
                }
            } else {
                // Show toast if there is no connectivity
                Toast.makeText(context, context.getResources().getString(R.string.noConnectivityMsg), Toast.LENGTH_LONG).show();
            }
        }
    }

}

