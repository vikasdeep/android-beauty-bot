package com.vikasdeep.beautybot;

import android.annotation.SuppressLint;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import ai.api.AIConfiguration;
import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;

/**
 * Main activity of Beauty Bot app
 */
public class MainActivity extends AppCompatActivity {

    private EditText mUserInput;
    private ChatAdapter mChatAdapter;
    private AIDataService mAIDataService;
    private ConnectivityStatusReceiver mConnectivityStatusReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // AI configuration
        AIConfiguration aiConfiguration = new AIConfiguration(getResources().getString(R.string.clientAccessToken), AIConfiguration.SupportedLanguages.English);
        mAIDataService = new AIDataService(aiConfiguration);

        mUserInput = findViewById(R.id.input_msg);

        ListView conversationList = findViewById(R.id.chat_list);
        conversationList.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        conversationList.setStackFromBottom(true);

        ArrayList<ChatData> chatlist = new ArrayList<>();
        mChatAdapter = new ChatAdapter(this, chatlist);
        conversationList.setAdapter(mChatAdapter);

        mConnectivityStatusReceiver = new ConnectivityStatusReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mConnectivityStatusReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mConnectivityStatusReceiver != null) {
            // unregister receiver
            unregisterReceiver(mConnectivityStatusReceiver);

            // remove the shared preference
            SharedPreferences.Editor editor = getApplication().getSharedPreferences(ConnectivityStatusReceiver.MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.remove(ConnectivityStatusReceiver.KEY_NAME).apply();
        }
    }

    // Perform action on the text entered when send button is clicked
    public void onSendButtonClick(View view) {

        String text = mUserInput.getText().toString();
        if (!text.trim().equals("")) {
            addMessage(text, true);
            sendMessage(text);
            mUserInput.setText("");
        }
    }

    // Add message to the screen based on the message if user's or agent's
    private void addMessage(String message, boolean isUser) {

        final ChatData chatMessage = new ChatData("", "", "", isUser);
        chatMessage.body = message;
        mChatAdapter.add(chatMessage);
        mChatAdapter.notifyDataSetChanged();
    }

    // Send message to Dialogflow server and to get response of that message
    private void sendMessage(String userText) {

        final String contextString = String.valueOf(userText);

        @SuppressLint("StaticFieldLeak") final AsyncTask<String, Void, AIResponse> task = new AsyncTask<String, Void, AIResponse>() {

            private AIError aiError;

            @Override
            protected AIResponse doInBackground(final String... params) {
                final AIRequest request = new AIRequest();
                String query = params[0];

                if (!TextUtils.isEmpty(query))
                    request.setQuery(query);

                try {
                    return mAIDataService.request(request);
                } catch (AIServiceException error) {
                    aiError = new AIError(error);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(final AIResponse response) {
                if (response != null) {
                    onResult(response);
                } else {
                    onError(aiError);
                }
            }
        };

        task.execute(contextString);
    }

    // When response is successfully received from Dialogflow server
    private void onResult(final AIResponse response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Result result = response.getResult();
                final String speech = result.getFulfillment().getSpeech();

                if (!TextUtils.isEmpty(speech))
                    addMessage(speech, false);
            }

        });
    }

    // Display toast notification if error is happened
    private void onError(final AIError error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
