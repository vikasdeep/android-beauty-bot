package com.vikasdeep.beautybot;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * ChatAdapter for binding data to ListView and to set message on UI based upon if user's message or agent's message
 */
public class ChatAdapter extends BaseAdapter {

    private static LayoutInflater mLayoutInflater = null;
    private ArrayList<ChatData> mChatMessageList;

    ChatAdapter(Activity activity, ArrayList<ChatData> list) {
        mChatMessageList = list;
        mLayoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public void add(ChatData object) {
        mChatMessageList.add(object);
    }

    @Override
    public int getCount() {
        return mChatMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"InflateParams", "RtlHardcoded"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ChatData message = mChatMessageList.get(position);
        View view = convertView;
        if (convertView == null)
            view = mLayoutInflater.inflate(R.layout.message, null);


        TextView msg = view.findViewById(R.id.text_msg);

        msg.setText(message.body);
        LinearLayout layout = view
                .findViewById(R.id.msg_layout);
        LinearLayout parent_layout = view
                .findViewById(R.id.msg_layout_parent);

        // messages position in ListView based on user's and agent's messages
        if (message.isUser) {
            layout.setGravity(Gravity.RIGHT);
            parent_layout.setGravity(Gravity.RIGHT);
            msg.setBackgroundResource(R.drawable.outgoing_msg);
        } else {
            layout.setGravity(Gravity.LEFT);
            parent_layout.setGravity(Gravity.LEFT);
            msg.setBackgroundResource(R.drawable.incoming_msg);
        }
        return view;
    }

}
